#Formación Frameworks RIA multiplataforma - Lleida

##Ejercicio Facturas Bootstrap
Ejercicio sobre la aplicación de clases Bootstrap a un HTML y CSS ya montados.

###Aclaración sobre el error
El error que nos daba, _jQuery is not defined_, era porque las rutas en Windows van con la barra invertida, y la parte del webpack.config.js que cargaba el jQuery llevaba las normales. Ya están incluidas las dos.

###Cómo descargarla
Si quieres usar Git, copia la URL que tienes más arriba, al lado de **HTTPS**, y haz un **git clone** desde tu disco duro.

Si quieres descargarte un zip, ve a la opción **Downloads** del menú izquierdo.

Si es la primera vez que la descargas, o si se ha añadido algún cambio en el archivo package.json, recuerda lanzar **npm install** después de descargarla.